# BOOK MANAGEMENT SYSTEM app

### About the app

*//DEVELOPMENT IN PROGRESS//*

*Initial tasks to complete:*
<br/>- One window for CRUD operations on Author (firstname, lastname): allows the viewing of all the authors; adding a new author; deleting an existing author and updating an existing author
<br/>- One window for CRUD operations on Book (title, description, author): allows the viewing of all the books; adding a new book - and assigning one of the existing authors; deleting an existing book and updating an existing book
<br/>- One window for Reviews (book, score, comment): allows the viewing of all the reviews; adding a new review for one of the existing books

### How to run
//TODO

### Tech used
*Java 11, Apache Maven, JavaFX, Hibernate, MySQL*
