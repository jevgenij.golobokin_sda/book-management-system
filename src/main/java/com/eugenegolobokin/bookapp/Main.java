package com.eugenegolobokin.bookapp;

import com.eugenegolobokin.bookapp.model.Author;
import com.eugenegolobokin.bookapp.model.Book;
import com.eugenegolobokin.bookapp.model.Review;
import com.eugenegolobokin.bookapp.util.HibernateUtil;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.hibernate.Session;

public class Main extends Application {

    double xOffset;
    double yOffset;

    public static void main(String[] args) {
        launch(args);
    }


    @Override
    public void start(Stage primaryStage) throws Exception {

        //create initial database with his method
        //createInitialDB();

        Parent root = FXMLLoader.load(getClass().getResource("/fxml/BookAppUI.fxml"));

        Scene scene = new Scene(root);

        root.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = primaryStage.getX() - event.getScreenX();
                yOffset = primaryStage.getY() - event.getScreenY();
            }
        });

        root.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                primaryStage.setX(event.getScreenX() + xOffset);
                primaryStage.setY(event.getScreenY() + yOffset);
            }
        });

        primaryStage.setTitle("Mr. Bookman");
        primaryStage.setScene(scene);
        primaryStage.initStyle(StageStyle.TRANSPARENT);
        primaryStage.show();

    }

    private void createInitialDB() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        Author author1 = new Author()
                .setFirstName("Ray")
                .setLastName("Bradbury");
        Book book1 = new Book()
                .setTitle("Fahrenheit 451")
                .setDescription("Guy Montag is a fireman. His job is to destroy the most " +
                        "illegal of commodities, the printed book, along with the houses in " +
                        "which they are hidden. Montag never questions the destruction and " +
                        "ruin his actions produce, returning each day to his bland life and " +
                        "wife, Mildred, who spends all day with her television “family.” But " +
                        "when he meets an eccentric young neighbor, Clarisse, who introduces " +
                        "him to a past where people didn’t live in fear and to a present where " +
                        "one sees the world through the ideas in books instead of the mindless " +
                        "chatter of television, Montag begins to question everything he has ever known.")
                .setImgUrl("https://images-na.ssl-images-amazon.com/images/I/71OFqSRFDgL.jpg");
        Review review1 = new Review()
                .setScore(5L)
                .setComment("Great book!");
        Review review2 = new Review()
                .setScore(3L)
                .setComment("Wasn't happy after reading, could be much better.");

        author1.addBook(book1);
        book1.addReview(review1);
        book1.addReview(review2);

        session.save(author1);
        session.save(book1);
        session.save(review1);
        session.save(review2);

        Author author2 = new Author()
                .setFirstName("George")
                .setLastName("Orwell");
        Book book2 = new Book()
                .setTitle("1984")
                .setDescription("The year is 1984. The country is impoverished and permanently " +
                        "at war, people are watched day and night by Big Brother and their " +
                        "every action and thought is controlled by the Thought Police. Winston " +
                        "Smith works in the department of propaganda, where his job is to rewrite " +
                        "the past. Spurred by his longing to escape, Winston rebels. He breaks " +
                        "the law by falling in love with Julia, and, as part of the clandestine " +
                        "organization the Brotherhood, they attempt the unimaginable – to bring " +
                        "down the Party.")
                .setImgUrl("https://m.media-amazon.com/images/I/41E9Z5XaHcL.jpg");
        Review review3 = new Review()
                .setScore(5L)
                .setComment("Great book!");

        author2.addBook(book2);
        book2.addReview(review3);

        session.save(author2);
        session.save(book2);
        session.save(review3);

        Author author3 = new Author()
                .setFirstName("Aldous ")
                .setLastName("Huxley");
        Book book3 = new Book()
                .setTitle("Brave New World")
                .setDescription("Brave New World is a dystopian novel by British author Aldous Huxley. " +
                        "Set in a futuristic World State, whose citizens are environmentally engineered " +
                        "into an intelligence-based social hierarchy, the novel anticipates huge scientific " +
                        "advancements in reproductive technology, sleep-learning, psychological manipulation " +
                        "and classical conditioning that are combined to make a dystopian society which is " +
                        "challenged by only a single individual: the story's protagonist; Bernard Marx. " +
                        "Bernard Marx seems alone harbouring an ill-defined longing to break free. A visit " +
                        "to one of the few remaining Savage Reservations where the old, imperfect life " +
                        "still continues, may be the cure for his distress...Huxley's ingenious fantasy " +
                        "of the future sheds a blazing light on the present and is considered to be his " +
                        "most enduring masterpiece.")
                .setImgUrl("https://m.media-amazon.com/images/I/51UZ0dj8EFL.jpg");
        Review review4 = new Review()
                .setScore(5L)
                .setComment("Great!");

        author3.addBook(book3);
        book3.addReview(review4);

        session.save(author3);
        session.save(book3);
        session.save(review4);

        session.getTransaction().commit();
        session.close();
    }

    /*
    @Override
    public void init() throws Exception {
        System.out.println("Loading ...");
        HibernateUtil.getSessionFactory().openSession() ;
    }*/

    @Override
    public void stop() throws Exception {
        System.out.println("Closing ...");
        HibernateUtil.shutdown();
    }
}
