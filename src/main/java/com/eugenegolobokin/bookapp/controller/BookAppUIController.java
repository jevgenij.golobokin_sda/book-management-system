package com.eugenegolobokin.bookapp.controller;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.layout.BorderPane;

import java.net.URL;
import java.util.ResourceBundle;


public class BookAppUIController implements Initializable {

    @FXML
    BorderPane workArea;
    long bookIdForReview;

    @FXML
    public void exitApp(ActionEvent event) {
        System.exit(0);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        loadBookLibraryWindow();
    }

    protected void loadBookInfoWindow(long bookId) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/BookInfo.fxml"));
            Parent root = loader.load();
            BookInfoController bookInfoController = loader.getController();
            // transferring an object data from this scene to another
            bookInfoController.setBookId(bookId);
            bookInfoController.setParentController(this);
            bookInfoController.loadBook();

            //Parent root = FXMLLoader.load(getClass().getResource("/fxml/BookInfo.fxml"));
            workArea.setCenter(root);

        } catch (Exception exception) {
            exception.printStackTrace();
        }

    }

    @FXML
    protected void loadBookLibraryWindow() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/BookLibrary.fxml"));
            Parent root = loader.load();
            BookLibraryController bookLibraryController = loader.getController();
            bookLibraryController.setParentController(this);
            bookLibraryController.populateWithBooks();
            workArea.setCenter(root);

        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    @FXML
    public void loadAuthorsWindow() {

    }

    @FXML
    public void loadReviewsWindow() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/Review.fxml"));
            Parent root = loader.load();
            ReviewController reviewController = loader.getController();
            reviewController.setParentController(this);
            reviewController.setBookId(bookIdForReview);
            reviewController.populateWithReviews();
            workArea.setCenter(root);

        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    @FXML
    public void loadNewBookWindow() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/NewBookInput.fxml"));
            Parent root = loader.load();
            NewBookInputController newBookInputController = loader.getController();
            newBookInputController.setParentController(this);
            workArea.setCenter(root);

        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    public void setBookIdForReview(long bookIdForReview) {
        this.bookIdForReview = bookIdForReview;
    }

    //TODO more generic method
//    private void loadUI(String ui) {
//         //root = null;
//        try {
//            Parent root = FXMLLoader.load(getClass().getResource("/fxml/"+ui+".fxml"));
//            workArea.setCenter(root);
//        } catch (IOException ioException) {
//            ioException.printStackTrace();
//        }
//
//    }

    //TODO
//    private <T> void loadUI(String ui, T something) {
//        Parent root = null;
//        Node node = (Node) event.getSource();
//
//        try {
//            root = FXMLLoader.load(getClass().getResource("/fxml/"+ui+".fxml"));
//        } catch (IOException ioException) {
//            ioException.printStackTrace();
//        }
//        borderPane.setCenter(root);
//    }
}
