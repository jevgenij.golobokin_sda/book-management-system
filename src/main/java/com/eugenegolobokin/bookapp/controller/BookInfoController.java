package com.eugenegolobokin.bookapp.controller;

import com.eugenegolobokin.bookapp.dao.BookDao;
import com.eugenegolobokin.bookapp.model.Book;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.awt.event.ActionEvent;
import java.net.URL;
import java.util.ResourceBundle;

public class BookInfoController implements Initializable {

    BookAppUIController parentController;
    private long bookId;
    BookDao bookDao = new BookDao();
    Book book = new Book();

    @FXML
    private ImageView bookCover = new ImageView();
    @FXML
    private TextField bookTitleFld;
    @FXML
    private TextField bookAuthorsFld;
    @FXML
    private TextArea bookDescriptionFld;
    @FXML
    private Button editBookBtn;
    @FXML
    private Button applyBtn;
    @FXML
    private Button cancelBtn;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }

    public void setBookId(long bookId) {
        this.bookId = bookId;
    }

    public long getBookId() {
        return bookId;
    }

    public void setParentController(BookAppUIController parentController) {
        this.parentController = parentController;
    }

    public void loadBook() {
        book = bookDao.getBook(this.bookId);
        bookCover.setImage(new Image(book.getImgUrl()));
        bookCover.setFitWidth(200);
        bookCover.setFitHeight(284);
        bookTitleFld.setText(book.getTitle());
        bookAuthorsFld.setText(book.getAuthorList().toString());
        bookDescriptionFld.setText(book.getDescription());
    }

    @FXML
    public void back() {
        parentController.loadBookLibraryWindow();
    }

    @FXML
    public void editBook() {
        bookTitleFld.setEditable(true);
        bookAuthorsFld.setEditable(true);
        bookDescriptionFld.setEditable(true);
        applyBtn.setOpacity(1.0);
        applyBtn.setDisable(false);
        applyBtn.setOnAction(apply -> applyChanges());
        cancelBtn.setOpacity(1.0);
        cancelBtn.setDisable(false);
        cancelBtn.setOnAction(cancel -> reloadWindow());
    }

    @FXML
    private void reloadWindow() {
        bookTitleFld.setEditable(false);
        bookAuthorsFld.setEditable(false);
        bookDescriptionFld.setEditable(false);
        applyBtn.setOpacity(0.0);
        applyBtn.setDisable(true);
        cancelBtn.setOpacity(0.0);
        cancelBtn.setDisable(true);
        loadBook();
    }

    @FXML
    private void applyChanges() {
        book.setTitle(bookTitleFld.getText());
        book.setDescription(bookDescriptionFld.getText());
        bookDao.updateBook(book);
        reloadWindow();
    }

    @FXML
    private void deleteBook() {
        bookDao.deleteBook(book);
        parentController.loadBookLibraryWindow();
    }

    @FXML
    private void seeReviews() {
        parentController.setBookIdForReview(this.bookId);
        parentController.loadReviewsWindow();
    }

}
