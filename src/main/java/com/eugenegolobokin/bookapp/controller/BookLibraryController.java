package com.eugenegolobokin.bookapp.controller;

import com.eugenegolobokin.bookapp.dao.BookDao;
import com.eugenegolobokin.bookapp.model.Book;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class BookLibraryController implements Initializable {

    BookAppUIController parentController;
    ObservableList<Book> bookList;
    @FXML
    private FlowPane bookField;
    @FXML
    private Button newBookBtn;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
    }

    public void setParentController(BookAppUIController parentController) {
        this.parentController = parentController;
    }

    protected void populateWithBooks() {
        List<Button> bookButtons = new ArrayList<>();
        getAllBooks();
        for (Book book : bookList) {
            ImageView bookCover = new ImageView(book.getImgUrl());
            bookCover.setFitWidth(150);
            bookCover.setFitHeight(204);
            Button button = new Button("", bookCover);
            button.setMaxSize(150.0, 204.0);
            button.setStyle("-fx-background-color: #212121; -fx-border-width: 0;");
            button.setOnAction(event -> parentController.loadBookInfoWindow(book.getId()));
            bookButtons.add(button);
        }
        bookField.getChildren().clear(); //remove all Buttons that are currently in the container
        bookField.getChildren().addAll(bookButtons);

    }

    private void getAllBooks() {
        BookDao bc = new BookDao();
        bookList = FXCollections.observableList(bc.getAllBooks());
    }

    @FXML
    private void addNewBook() {
        parentController.loadNewBookWindow();
    }

}
