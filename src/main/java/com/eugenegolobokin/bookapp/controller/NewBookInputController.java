package com.eugenegolobokin.bookapp.controller;

import com.eugenegolobokin.bookapp.dao.BookDao;
import com.eugenegolobokin.bookapp.model.Author;
import com.eugenegolobokin.bookapp.model.Book;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.HashSet;
import java.util.ResourceBundle;

public class NewBookInputController implements Initializable {

    BookAppUIController parentController;
    BookDao bookDao = new BookDao();
    Book book = new Book();

    @FXML
    private TextField bookTitleFld;
    @FXML
    private TextField authorNameFld;
    @FXML
    private TextField authorSurnameFld;
    @FXML
    private TextField bookCoverUrlFld;
    @FXML
    private TextArea bookDescriptionFld;
    @FXML
    private TextField alertTxtFld;
    @FXML
    private Button saveBtn;
    @FXML
    private Button cancelBtn;

    @FXML
    void back() {
        parentController.loadBookLibraryWindow();
    }

    @FXML
    void saveNewBook() {
        if (checkForEmptyFields()) {
            book.setTitle(bookTitleFld.getText());
            Author author = new Author();
            author.setFirstName(authorNameFld.getText());
            author.setLastName(authorSurnameFld.getText());
            book.setAuthorList(new HashSet<>() {
                {
                    add(author);
                }
            });
            book.setDescription(bookDescriptionFld.getText());
            book.setImgUrl(bookCoverUrlFld.getText());
            bookDao.createBook(book);
            parentController.loadBookLibraryWindow();
        } else {
            alertTxtFld.setText("All fields must be filled in!!");
        }
    }

    private boolean checkForEmptyFields() {
        if (bookTitleFld.getText().trim().isEmpty() ||
                authorNameFld.getText().isEmpty() ||
                authorSurnameFld.getText().isEmpty() ||
                bookDescriptionFld.getText().isEmpty() ||
                bookCoverUrlFld.getText().isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    public void setParentController(BookAppUIController parentController) {
        this.parentController = parentController;
    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }
}
