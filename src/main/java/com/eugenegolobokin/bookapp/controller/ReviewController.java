package com.eugenegolobokin.bookapp.controller;

import com.eugenegolobokin.bookapp.dao.BookDao;
import com.eugenegolobokin.bookapp.dao.ReviewDao;
import com.eugenegolobokin.bookapp.model.Book;
import com.eugenegolobokin.bookapp.model.Review;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class ReviewController implements Initializable {

    BookAppUIController parentController;
    private long bookId;
    private Book book;
    ObservableList<Review> reviewList;
    @FXML
    private ImageView bookCover;
    @FXML
    private VBox reviewHolder;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }

    public void setParentController(BookAppUIController parentController) {
        this.parentController = parentController;
    }

    private void getAllReviews() {
        ReviewDao rd = new ReviewDao();
        reviewList = FXCollections.observableList(rd.getAllReviews(bookId));
    }

    protected void populateWithReviews() {
        List<TextField> reviewFields = new ArrayList<>();
        setBook(bookId);
        getAllReviews();
        bookCover.setImage(new Image(book.getImgUrl()));
        bookCover.setFitWidth(200);
        bookCover.setFitHeight(284);
        for (Review review : reviewList) {
            TextField textFieldPre = new TextField();
            TextField textFieldRating = new TextField("Rating: " + review.getScore() + "/5");
            TextField textFieldComment = new TextField(review.getComment());
            reviewFields.add(textFieldPre);
            reviewFields.add(textFieldRating);
            reviewFields.add(textFieldComment);
        }
        reviewHolder.getChildren().clear();
        reviewHolder.getChildren().addAll(reviewFields);
    }

    @FXML
    private void deleteReview() {

    }

    @FXML
    private void back() {
        parentController.loadBookInfoWindow(bookId);
    }

    public void setBookId(long bookId) {
        this.bookId = bookId;
    }

    public void setBook(long bookId) {
        BookDao bd = new BookDao();
        this.book = bd.getBook(bookId);
    }
}
