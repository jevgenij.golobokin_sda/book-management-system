package com.eugenegolobokin.bookapp.dao;

import com.eugenegolobokin.bookapp.model.Book;
import com.eugenegolobokin.bookapp.model.Review;
import com.eugenegolobokin.bookapp.util.HibernateUtil;
import org.hibernate.Session;

import java.util.List;

public class ReviewDao {

    public List<Review> getAllReviews(long bookId) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            return session.createQuery("from Review where book_id = :bookId", Review.class).setParameter("bookId", bookId).list();
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        } finally {
            session.close();
        }
    }

}
