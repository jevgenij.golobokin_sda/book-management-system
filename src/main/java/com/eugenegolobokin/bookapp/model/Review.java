package com.eugenegolobokin.bookapp.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Entity
@Getter
@Setter
@ToString
@Accessors(chain = true)
public class Review {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long score;
    @Column(length = 1500)
    private String comment;

    @ManyToOne
    @JoinColumn(name = "book_id")
    private Book book;
}
